<div class="modal-content">
    <div class="modal-header">
        <h2 class="modal-title" id="contact">Get in touch</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="alert alert-dismissible disp-none" role="alert" id='results'>
        </div>
        <div class="contact-form-area">
            <form id="contact" action="#">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Name (max 25)" maxlength="25" required="required">
                    </div>
                    <div class="col-12 col-md-6">
                        <input type="email" name="email" class="form-control" id="email" placeholder="E-mail (max 50)" maxlength="50" required="required">
                    </div>
                    <div class="col-12">
                        <input type="text" name="subject" class="form-control" id="subject" placeholder="Subject (max 100)" maxlength="100" required="required">
                    </div>
                    <div class="col-12">
                        <textarea name="message" class="form-control" id="message" cols="30" rows="10" placeholder="Message (max 500)" maxlength="500" required="required"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn mosh-btn-second mt-50" data-dismiss="modal">Close</button>
                    <button class="btn mosh-btn mt-50 btn-anim" type="submit" id="button">Send Message</button>
                </div>
            </form>
        </div>
    </div>
</div>