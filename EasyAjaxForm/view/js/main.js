$( document ).ready(function() {

//*  AJAX FORM *//
	$(document).on('click', "[data-target='#contact']", function(e){
		e.preventDefault();
		var $form_div = $('.ajax-modal'); //loading a form in to this div.class
		var $form_class = '.modal-content'; //checking loaded form
		var $path_to_form = "/easyajaxform/view/contactForm.php"; // path to the form template
		// if the form have loaded, don't load the form again
		
		if ($form_div.find($form_class).length !== 1) {
			load_form($form_div, $path_to_form);
		}       
	});

	$(document).on( "submit", "form#contact", function(event) {
		event.preventDefault();
		// variables
		var $formFields = ['email', 'textarea', 'text', 'submit'];
		var $button = 'form button#button';
		var $SendToHandle = '/easyajaxform/handler/contactForm.php';
		var $validClass = 'valid';
		var $no_validClass = 'no-valid';
		var $buttonText = 'Send Message'; 
		var $buttonTextSended = 'Sended';
		var $buttonLoadingAnimationClass = 'onclic';
		var $resultContainer = '#results';
		var $buttonLockTime = 2000;
		//data
		var $msg = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: $SendToHandle,
			data: $msg,
			beforeSend: function() {
				$(this).ready(function() {
					//disable all inputs
					blockInput($formFields, true);
					$( $button ).addClass( $buttonLoadingAnimationClass );
					$( $resultContainer ).slideUp();
					$( $button ).text();
				});
		  },

		  success: function(data) {
			//result
			$( $resultContainer ).html( data );
			// Scroll to top
			scrolling($('#contact'), 100, 500);
			//animation
			$( $button ).removeClass( $buttonLoadingAnimationClass );
			//if everything is good
			if ($( $resultContainer + " span[data='success']").length > 0) {
				$( $button ).addClass( $validClass );
				addAndRemoveClass($resultContainer, "alert-danger", "alert-success");
				$( $button ).text( $buttonTextSended );
			} else {
				addAndRemoveClass($resultContainer, '', "alert-danger");
				$( $button ).text( $buttonText );
				$( $button ).addClass( $no_validClass );
				setTimeout(function() {
					$( $button ).removeClass( $no_validClass );
					blockInput( $formFields );
				}, $buttonLockTime );
			}
		  },
		  error:  function(xhr, str) {
			alert('Error. Try later or contact me by email: alexwebdevua@gmail.com');
		  }
		});
	})

});

//* FUNCTIONS *//

	//*  AJAX FORM  *//
		//disable/enable inputs AJAX
		function blockInput($formFields, $action = false) {
			$("form input[type="+$formFields[0]+"], "+$formFields[1]+", input[type="+$formFields[2]+"], button[type="+$formFields[3]+"]").attr("disabled", $action);
		}
		//add and remove classes to containter
		function addAndRemoveClass( $container, $classRemove = '', $classAdd = '') {
			$( $container ).removeClass( $classRemove ).addClass( $classAdd ).slideToggle();
		}
		// Loading the form
		function load_form(id_div_load, path_to_form) {
			$(id_div_load).load(path_to_form);
		}
		// clear the form
		function clear_form(remove, time = 200) {
			setTimeout(function() {
				$(remove).empty();
			}, time);
		}

	//*  ANIMATIONS *//
		function scrolling ($div, $position, $timeAnim) {
			$div.stop().animate({scrollTop: $position}, $timeAnim);
		}