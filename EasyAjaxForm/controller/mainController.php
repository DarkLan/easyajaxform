<?php
if(!defined('ACCESS')) die('access denied');

abstract class Main {	
	protected $lang;
	protected $langText;

	private	function __construct(string $lang) {
		$this->lang = $lang;
		$this->requireLang($this->lang);
	}

	//Language file
	private function requireLang(string $lang) {
		require_once(__ROOT_DIR__.__MOD_DIR__.'/lang/'.$lang.'.php');
		$this->langText = $errorsText;
	}

	public function create(string $lang = 'eng') {
		return new static($lang);
	}
}