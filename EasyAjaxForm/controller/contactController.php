<?php
if(!defined('ACCESS')) die('access denied');
/**
* VALIDATION
*/

// main class
require_once('mainController.php');

class Validate extends Main
{
	private $strlen = 2;
	private $fieldNames = array();
	private $Strings = false;
	/*
	array $string what fields check for length
	*/
	public function run(array $datas, array $strings = null) {
		$this->fieldNames = array_keys($datas);
		if ($strings !== null) $this->Strings = $strings;

		$errors['errors'] = array_merge($this->ValidFields($datas, 'StrEmpty', 'StrSmall'), $this->emailValid($datas['email'], 'EmailNoValid'));
		
		return $errors;
	}

	/*
	SetUp $strlen for length validation
	*/
	protected function setStrlen (ineger $val) {
		$this->strlen = $val;
	}
	// The length of fields validation.
	/*
	$data array(). Form datas (fields)
	$string array(). name of fields for length validation. Parametr: private $strlen
	$lngMessageKey string.	Parametr for language (a key for {lang}.php)
	*/
	private function lenString(array $datas, array $strings, $lngMessageKey) {
		foreach ($datas as $key => $value) {
			if (in_array($key, $strings)) {
				if (iconv_strlen($value) <= $this->strlen) $errors[$key] = sprintf($this->langText[$lngMessageKey], $key);
			}
		}
		if (!isset($errors)) return array();
		return $errors;
	}

	// Email validation.
	/*
	$email string. Email for validation
	$lngMessageKey string.	Parametr for language (a key for {lang}.php)
	*/
	private function emailValid(string $email, string $lngMessageKey) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors['email'] = sprintf($this->langText[$lngMessageKey], 'email');
		
		if (!isset($errors)) return array();
		return $errors;
	}

	// Fields validation.
	/*
	$data array(). Form datas (fields)
	$string array(). name of fields for length validation. Parametr: private $strlen
	$lngMessageKeyEmpty, $lngMessageKeyLen string.	Parametr for language (a key for {lang}.php)
	*/
	private function ValidFields(array $datas, string $lngMessageKeyEmpty, string $lngMessageKeyLen) {
		
		if (count($datas) <= 0) return $errors['server'] = sprintf($this->langText['emptyDatas'], 'server');

		for ($key=0; count($datas) > $key; $key++):
			if (empty($datas[$this->fieldNames[$key]])) :
				$errors[$this->fieldNames[$key]] = sprintf($this->langText[$lngMessageKeyEmpty], $this->fieldNames[$key]);
			endif;
		endfor;

		if (!isset($errors)):
			if ($this->Strings !== false) :
				//if need length validation
				return $this->lenString($datas, $this->Strings, $lngMessageKeyLen);
			endif;
			return array();
		endif;
			return $errors;
		
	}
}
