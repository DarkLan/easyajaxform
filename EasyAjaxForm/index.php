<?php
require_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR.'/easyajaxform/config.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>EaseAjaxForm</title>
	<link rel="icon" href="<?php echo __MOD_DIR__?>/view/img/core-img/favicon.ico">

	<!-- Core Stylesheet -->
	<link href="<?php echo __MOD_DIR__?>/view/css/style.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="<?php echo __MOD_DIR__?>/view/css/responsive.css" rel="stylesheet">


</head>
<body>
	<button type="button" class="btn btn-primary mosh-btn" data-toggle="modal" data-target="#contact">ClickME</button>
	<!-- ***** Contact Area Start ***** -->
		<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="contact" aria-hidden="true">
			<div class="ajax-modal modal-dialog modal-dialog-centered" role="document"></div>
		</div>
	<!-- ***** Contact Area End ***** -->
</body>

	<!-- jQuery-2.2.4 js -->
	<script src="<?php echo __MOD_DIR__?>/view/js/jquery-2.2.4.min.js"></script>
	<!-- Popper js -->
	<script src="<?php echo __MOD_DIR__?>/view/js/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="<?php echo __MOD_DIR__?>/view/js/bootstrap.min.js"></script>
	<!-- MainJS -->
	<script src="<?php echo __MOD_DIR__?>/view/js/main.js"></script>
</html>