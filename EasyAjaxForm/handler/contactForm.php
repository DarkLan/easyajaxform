<?php
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] !== 'XMLHttpRequest') return;
define('ACCESS', true);

define('__MOD_DIR__', '/easyajaxform'); 
define('__ROOT_DIR__', $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR);

require_once(__ROOT_DIR__.__MOD_DIR__.'/controller/contactController.php'); //controller

/* It's (create()) accept a lang parameter by string from lang directory. 
ex: create('eng') and you have to create a file "eng.php" in lang directory
default: eng.
*/
$mainObject = Validate::create();

//Just for loading effect
sleep(2);

// max fields of your form
if (count($_POST) < 4) : 
	echo 'server error'; 
	return ; 
endif;

// Datas
$datas = [
	'name' => trim(htmlspecialchars($_POST['name'])),
	'email' => trim(htmlspecialchars($_POST['email'])),
	'subject' => trim(htmlspecialchars($_POST['subject'])),
	'message' => trim(htmlspecialchars($_POST['message']))
];

// Validation
$errorsArray = $mainObject->run($datas, ['name', 'subject', 'message']);

if (!empty($errorsArray['errors'])) : 
	foreach ($errorsArray['errors'] as $errors) :
		echo $errors.'<br>';
	endforeach;
	return;
endif;

// if everything are good.
	echo "<span data='success'>I've got the message.</span>";