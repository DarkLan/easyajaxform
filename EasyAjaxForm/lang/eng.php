<?php
// errors text
$errorsText = [
	'StrEmpty' => 'Please, enter your <strong>%s</strong>',
	'EmailNoValid' => 'The <strong>%s</strong> you entered is not valid',
	'StrSmall' => 'The <strong>%s</strong> looks too small. Type a bit more :)',
	// server errors
	'emptyDatas' => 'Something wrong on %s. Please try later...'
];